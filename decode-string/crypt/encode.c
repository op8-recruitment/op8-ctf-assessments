#include <stdlib.h>
#include <string.h>
#include <stdio.h>

unsigned char *encode(unsigned char *input) {
	unsigned char *string = NULL;
	unsigned int length = 0;
	for(unsigned int i = 0; i < strlen(input); i++) {
		unsigned char *newPtr = realloc(string, length + 3);
		string = newPtr;
		length += sprintf(string + length, "%03d", input[i]);
	}
	return string;
}
