#include <stdio.h>
#include <stdlib.h>
#include "crypt/encode.h"

int main(int argc, char *argv[]) {

	if(argc < 2) {
		printf("\n\033[4mOP8 Reverse Engineering Assessment #1\033[24m\n\n");
		printf("This tool is used to encode a string.\n\n");
		printf("Decode the encoded string within README.md in the current directory.\n\n");
		printf("Usage: %s \"<string>\"\n\n", argv[0]);
		exit(1);
	}

	printf("%s\n", encode(argv[1]));

}
