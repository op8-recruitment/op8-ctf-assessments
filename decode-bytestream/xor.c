#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "crypt/xor.h"

int main(int argc, unsigned char *argv[]) {

	if(argc < 3) {
		printf("\n\033[4mOP8 Reverse Engineering Assessment #2\033[24m\n\n");
                printf("This tool is used to XOR a string with a multiple byte key.\n\n");
                printf("Decode the bytestream within README.md in the current directory to plaintext and submit the key and the plaintext.\n\n");
		printf("Usage: %s \"<key>\" \"<string>\"\n\n", argv[0]);
		exit(1);
	}

	unsigned char *result = xor(argv[1], argv[2]);

	unsigned int length = strlen(result);

	printf("\nEncrypted result is: \"");

	for(int i = 0; i < length; i++) {
		printf("0x%02hx", result[i]);
		if(i != length - 1) {
			printf(" ");
		}
	}

	printf("\"\n\n");
}
