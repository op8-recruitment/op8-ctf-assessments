# OP8 Reverse Engineering Assessment #2

This tool is used to encrypt a string.

Decode the encoded string within this README into plaintext and submit the key along with the decoded plaintext.

`Usage: ./encode "<key>" "<string>"`

```
0x16 0x1a 0x10 0x6c 0x0f 0x02
```
