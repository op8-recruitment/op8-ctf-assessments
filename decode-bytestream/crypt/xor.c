#include <string.h>
#include <stdlib.h>
#include <stdio.h>

unsigned char *xor(char *key, char *input) {

	unsigned int keyLength = strlen(key);
	unsigned int inputLength = strlen(input);

	unsigned char *output = malloc(inputLength);

	unsigned int counter = 0;

	for(int i = 0; i < inputLength; i++) {
		output[i] = input[i] ^ key[counter];
		if(counter == keyLength) {
			counter = 0;
		} else {
			counter++;
		}
	}

	return output;
}
